/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.lang.reflect.Constructor;
import org.apache.commons.cli.*;

public class Main {
    
    private static Options buildOptions() {
	Options options = new Options();

	Option projectName = OptionBuilder.withLongOpt("project")
	    .hasArg()
	    .withArgName("NAME")
	    .withDescription("project name Ex. 'EclipseXML' or 'android'")
	    .create();
	Option stemming = new Option("s", "stemming", false,
				     "perform stemming on all words" );
	Option parallel = new Option("p", "parallel", false,
				     "perform bug comparisons in parallel");
	Option help = new Option("h", "help", false, 
				 "prints argument descriptions");
	Option couchdb = new Option("c", "couchdb", false,
				    "use couchdb as backend");
	Option importantWords = OptionBuilder.withLongOpt("importantWords")
	    .hasArg()
	    .withArgName("FILENAME")
	    .withDescription("filename with a list of important words")
	    .create();
	Option userDict = OptionBuilder.withLongOpt("userDictionary")
	    .hasArg()
	    .withArgName("DIR")
	    .withDescription("name of directory containing user "
			     + "defined dictionaries")
	    .create();
	Option output = OptionBuilder.withLongOpt("output")
	    .hasArg()
	    .withArgName("DIRECTORY")
	    .withDescription("Output directory")
	    .create();
        Option sample = new Option("m", "sample", false, "produce the sample .csv " +
                                   "file used for regressions");

	options.addOption(projectName);
	options.addOption(stemming);
	options.addOption(parallel);
	options.addOption(help);
	options.addOption(couchdb);
	options.addOption(importantWords);
	options.addOption(userDict);
	options.addOption(output);
        options.addOption(sample);
	
	return options;
    }

    public static void main(String[] args) {
	Options options = buildOptions();

	CommandLineParser cliParser = new GnuParser();
	CommandLine line = null;
	try {
	    line = cliParser.parse( options, args );
	}
	catch( ParseException exp ) {
	    System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
	}

	if (line.hasOption("help")) {
	    HelpFormatter formatter = new HelpFormatter();
	    formatter.printHelp( "bug-deduplication", options );
	}

	String projectName = line.getOptionValue("project");
	Boolean stemming = line.hasOption("stemming");
	Boolean parallel = line.hasOption("parallel");
	String importantWords = line.hasOption("importantWords") ?
	    line.getOptionValue("importantWords") : "";
	String userDir = line.hasOption("userDictionary") ? 
	    line.getOptionValue("userDictionary") : "";
	String directory = line.hasOption("output") ? 
	    line.getOptionValue("output") : "out";
        String filename = directory + "/" + projectName + ".csv";
        Boolean isSample = line.hasOption("sample");

	System.err.println(String.format("Project name:%s, stemming:%b, "
					 + "parallel:%b, important words "
					 + "filename:%s, user defined dictionary "
					 + "directory:%s, output filename:%s", 
					 projectName, stemming, parallel, 
					 importantWords, userDir, filename));

	Dictionaries allDictionaries = new Dictionaries(projectName, userDir,
							stemming, importantWords);
	Processor processor = null;
	REPDumper dumper = new REPDumper(allDictionaries, stemming, 
					 filename);

        if (line.hasOption("couchdb"))
            processor = new IncrementalProcessor(dumper, projectName, directory);
        else
            processor = new ElasticProcessor(dumper, projectName, directory, isSample);

	processor.process(parallel);
    }
}