/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.text.ParseException;
import java.util.Formatter;
import java.util.Map;
import java.util.HashMap;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Loads and runs fastREP's logit model against bug-dedup output
 */
public class Logit {
    private String projectName;
    private String directory;
    private String[] CONTEXTS = {"cosine_architecture", "cosine_nfr",
                                 "cosine_junk", "cosine_lda", "cosine_user",
                                 "REP", "ALL"};
    public Map<String, Map<String, Double>> coefficients;

    public Logit(String projectName, String directory) {
        this.projectName = projectName;
        this.directory = directory;
    }

    public void loadLogitModels() {
        coefficients = new HashMap<String, Map<String, Double>>();
        for (String context: CONTEXTS) {
            coefficients.put(context, getLogitModel(context, true));
            if (context.equals("REP"))
                continue;
            coefficients.put("norep_" + context, getLogitModel(context, false));
        }
    }

    public Map<String, Double> getLogitModel(String column, Boolean rep) {
        String filename = getLogitFileName(column, rep);
        return readLogitFile(filename);
    }

    public String getLogitFileName(String column, Boolean rep) {
        String rep_string = rep? "" : "1";
        if (column.equals("ALL"))
            rep_string = rep? "0": "1";

        String context = column.equals("ALL")? "1": "";

        return String.format("8020.%s.%s.%s.lmfit-coef.csv", 
                             column, rep_string, context);
    }

    public Map<String, Double> readLogitFile(String filename) {
        BufferedReader br = null;
        String dirPlusFilename = directory + "/" + projectName + "/lmfit/" + 
            filename;
        File file = new File(dirPlusFilename);

        if (!file.exists())
            file = new File(directory + "/default/" + filename);

        Map<String, Double> coeffMap  = new HashMap<String, Double>();
        try {
            String line;
            FileReader reader = new FileReader(file);
            br = new BufferedReader(reader);
 
            while ((line = br.readLine()) != null) {
                /* remove all quotes and parenthesis */
                line = line.replaceAll("\"|\\(|\\)", "");
                String[] lineSplit = line.split(",");
                /* Only put coeff in map if it is a number */
                try {
                    Double coeff = Double.parseDouble(lineSplit[1]);
                    coeffMap.put(lineSplit[0], coeff);
                } catch (NumberFormatException e) {
                    continue;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return coeffMap;
    }

    public Map<String, Double> performAllLogits(Map<String, Double> results) {
        Map<String, Double> allLogits = new HashMap<String, Double>();
        for (String context: CONTEXTS) {
            allLogits.put(context, performLogit(results, context));
            if (context.equals("REP")) {
                continue;
            }
            allLogits.put("norep_" + context, performLogit(results, "norep_" + context));
        }
        return allLogits;
    }

    public Double performLogit(Map<String, Double> results, String context) {
        Map<String, Double> contextCoeff = coefficients.get(context);

        /* start with intercept */
        Double logit = 0.0;
        /* add on coeff * value for each coeff in context */        
        for (String key: contextCoeff.keySet()) {
            if (key.equals("Intercept")) {
                logit += contextCoeff.get(key);
            }
            else if (!results.containsKey(key))
                continue;
            else
                logit += (contextCoeff.get(key) * results.get(key));
        }
        return Math.exp(logit)/(Math.exp(logit) + 1);
    }
}