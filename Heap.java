/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.PriorityQueue;
import java.util.Map;
import java.util.HashMap;
import java.util.Comparator;

public class Heap {
    private int CAPACITY = 10000;
    public Map<String, PriorityQueue<HeapObject>> heaps;
    private String[] CONTEXTS = {"cosine_architecture", "cosine_nfr",
                                 "cosine_junk", "cosine_lda", "cosine_user",
                                 "REP", "ALL"};

    public Heap() {
        initHeaps();
    }

    /** 
     * Used for testing
     **/    
    public Heap(int capacity) {
        CAPACITY = capacity;
        initHeaps();
    }

    public void initHeaps() {
        heaps = new HashMap<String, PriorityQueue<HeapObject>>();
        for (String context: CONTEXTS) {
            heaps.put(context, new PriorityQueue<HeapObject>(CAPACITY, 
                                                             HeapObjectComparator));
            heaps.put("norep_" + context, 
                      new PriorityQueue<HeapObject>(CAPACITY, 
                                                    HeapObjectComparator));
        }
    }

    /**
     * Inserts an element into a context's heap.
     * Returns false if element was not inserted
     */
    public Boolean addToHeap(String context, Double value, String id1, String id2) {
        PriorityQueue<HeapObject> heap = heaps.get(context);
        HeapObject obj = new HeapObject(id1, id2, value);

        /* Heap hasn't hit capacity yet, add value regardless */
        if (heap.size() < CAPACITY)
            return heap.offer(obj);

        /* value is too small */
        HeapObject min = heap.peek();
        if (value < min.value) {
            return false;
        }

        /* sweet spot */
        heap.poll();
        return heap.offer(obj);
    }

    static final Comparator<HeapObject> HeapObjectComparator = 
        new Comparator<HeapObject>() {
        public int compare(HeapObject o1, HeapObject o2) {
            if (o1.value > o2.value)
                return 1;
            else
                return -1;
        }
    };

    public class HeapObject {
        String id1;
        String id2;
        Double value;

        public HeapObject(String id1, String id2, Double value) {
            this.id1 = id1;
            this.id2 = id2;
            this.value = value;
        }

    }
}