/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import com.google.gson.*;

public class InsertSimsOneUpdate {
    private static String project = "android";
    private static List<BufferedReader> readers = new ArrayList<BufferedReader>();
    private static String[] CONTEXTS = {"all", "architecture", "lda", "nfr", 
                                        "user", "junk", "norep_all", 
                                        "norep_architecture", "norep_lda", 
                                        "norep_nfr", "norep_user", 
                                        "norep_junk", "rep"};

    private static void initFileReaders() {
        for (String field: CONTEXTS) {
            File file = new File("/csv_files/" + project + "/" + field + ".csv");
            try {
                /* create new filewriter that appends */
                FileReader fr = new FileReader(file);
                readers.add(new BufferedReader(fr));
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void readFileAndInsert() {
	ElasticSearchManager es = new ElasticSearchManager(project);
        Gson gson = new Gson();

        /* skip header line */
        for (BufferedReader reader: readers) {
            try {
                reader.readLine();
            } catch (Exception e) {
                    e.printStackTrace();
            }
        }

        while(true) {
            JsonObject similarityObj = getLines();
            if (similarityObj != null) {
                String id = similarityObj.remove("id").getAsString();
                es.addSimilarity(gson.toJson(similarityObj), id);
            }
            else
                break;
        }
	/* execute any leftover actions in bulk */
	es.executeBulk();
	es.shutdown();
        closeReaders();
    }

    private static JsonObject getLines() {
        JsonObject similarityObj = new JsonObject();
        String id = "";
        for (int i = 0; i < CONTEXTS.length; i++) {
            BufferedReader reader = readers.get(i);
            try {
                String line = reader.readLine();
                if (line != null) {
                    String[] lineSplit = line.split(",");
                    int len = lineSplit.length;
                    if (i == 0) {
                        similarityObj.addProperty("id", lineSplit[0] + "_" 
                                                  + lineSplit[1]);
                        similarityObj.addProperty("id1", lineSplit[0]);
                        similarityObj.addProperty("id2", lineSplit[1]);
                    }
                    Double similarity = Double.parseDouble(lineSplit[len - 1]);
                    similarityObj.addProperty(CONTEXTS[i], similarity);
                }
                else
                    return null;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return similarityObj;
    }

    private static void closeReaders() {
        System.out.println("closing readers");
        for (BufferedReader reader: readers) {
            try {
                reader.close();
            } catch (Exception e) {
                    e.printStackTrace();
            }
        }

    }

    public static void main(String[] args) {
	if (args.length != 1)
	    System.out.println("Usage is <project>. " +
			       "Ex. java InsertSimilarities android");
        project = args[0];

	initFileReaders();
	readFileAndInsert();
    }
}