/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java .util.PriorityQueue;
import java.util.concurrent.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ElasticProcessor extends Processor {
    private ArrayList<Document> calculated;
    private ArrayList<Document> notCalculated;
    private static ElasticSearchManager es;
    private final ByteArrayOutputStream errContent = 
	new ByteArrayOutputStream();
    private Boolean isSample = true;
    private Logit logit;
    public Heap heap;

    public ElasticProcessor(REPDumper rd, String projectName, String directory) {
	super(rd);
	es = new ElasticSearchManager(projectName);
	rd.initFileWriter(false);
    }

    public ElasticProcessor(REPDumper rd, String projectName, String directory,
                            Boolean isSample) {
	super(rd);
	es = new ElasticSearchManager(projectName);
        this.isSample = isSample;
        if (isSample) {
            rd.initFileWriter(false);
        }
        else {
            logit = new Logit(projectName, directory);
            logit.loadLogitModels();
            heap = new Heap();
        }
    }

    public void process(Boolean parallel) {
        if (isSample) {
	    BugSampler sampler = new BugSampler(es, 1000);
            /* we want to recalculate everything if we're using a sample */
            calculated = new ArrayList<Document>();
            notCalculated = sampler.getDupeSample();
            notCalculated.addAll(sampler.getNonDupeSample());
        }
        else {
            calculated = es.getAlreadyCalculatedBugs();
            notCalculated = es.getNotCalculatedBugs();
        }

	calculated = rd.processDocsDupeStrategy(calculated);
	notCalculated = rd.processDocsDupeStrategy(notCalculated);

	int dupesCount = rd.getNumDups(calculated) 
	    + rd.getNumDups(notCalculated);
	
	/* Corpus needs to be made of all bugs */
	ArrayList<Document> allBugs = new ArrayList<Document>();
	allBugs.addAll(calculated);
	allBugs.addAll(notCalculated);
	rd.initCorpusAndREP(allBugs);
	allBugs = null;

	/* perform calculations for new docs */
	for (Document doc: notCalculated) {
	    Boolean calcsPerformed = rd.doIndividualDocCalcs(doc);
	    if (calcsPerformed)
		es.updateDoc(doc);
	}

	if (parallel)
	    parallelProcess();
	else
	    nonParallelProcess();
	
	/* make sure everything in the bulk queue is executed*/
	es.executeBulk();
	calculated = null;
	notCalculated = null;
	rd.cleanUpObjects();
        if (isSample)
            rd.closeWriter();
        else {
            for (String context: heap.heaps.keySet()) {
                PriorityQueue<Heap.HeapObject> queue = heap.heaps.get(context);
                context = context.replace("cosine_", "").toLowerCase();
                while (queue != null && queue.size() != 0) {
                    Heap.HeapObject heapObj = queue.poll();
                    es.updateSimilarity(context, heapObj.value, heapObj.id1, heapObj.id2);
                }
            }
        }
	es.shutdown();
    }
    
    public void nonParallelProcess() {
	/* old vs new */
	int count = 0;
	for (Document doc1 : calculated) {
	    for (Document doc2 : notCalculated) {
                processDump(doc1, doc2);
                count++;
	    }
	}

	/* new vs new */
	for (int i = 0; i < notCalculated.size() - 1; i++) {
	    Document doc1 = notCalculated.get(i);
	    for (int j = i + 1; j < notCalculated.size(); j++) {
		Document doc2 = notCalculated.get(j);
                processDump(doc1, doc2);
                count++;
	    }
	}
	System.out.println(count + " similarities added");
    }

    private void processDump(Document doc1, Document doc2) {
        if (isSample) {
            String dump = rd.dump(doc1, doc2);
            rd.writeToFile(dump);
        }
        else {                    
            /* do logit calcs, buckets */
            Map<String, Double> results = rd.dumpToMap(doc1, doc2);
            results = logit.performAllLogits(results);
            for (String context: results.keySet()) {
                heap.addToHeap(context, results.get(context), doc1.getBugID(), 
                               doc2.getBugID());
            }
        }
    }

    public void parallelProcess() {
	/* setup final variables */
	final ArrayList<Document> fcalculated = calculated;
	final ArrayList<Document> fnotCalculated = notCalculated;

	final LinkedBlockingQueue<ArrayList> queue = 
	    new LinkedBlockingQueue<ArrayList>();
	ExecutorService executor = getExecutor();
	int threadCount = 0;

	for (int i = 0; i < fnotCalculated.size(); i++) {
	    Document doc1 = fnotCalculated.get(i);
	    final Document fdoc1 = doc1;
	    final int di = i;
	    Thread t = new Thread(new Runnable() {
		    public void run() {
			ArrayList results = new ArrayList();
			String dump = "";
			for (Document doc2: fcalculated) {
			    /* new vs old */
                            processDump(results, fdoc1, doc2);
			}
			if (di == fnotCalculated.size() - 1) {
			    addResultsToQueue(results);
			    return;
			}
			for (int j = di + 1; j < fnotCalculated.size(); j++) {
			    /* new vs new */
			    Document doc3 = fnotCalculated.get(j);
                            processDump(results, fdoc1, doc3);
			}
			addResultsToQueue(results);
		    }

                    public void processDump(ArrayList results, Document doc1, 
                                            Document doc2) {
                        if (isSample) {
			    String dump = rd.dump(fdoc1, doc2);
			    results.add(dump);
                        }
                        else {
                            /* do logit calcs */
                            Map<String, Double> dump = rd.dumpToMap(doc1, doc2);
                            dump = logit.performAllLogits(dump);
                            results.add(new REPResults(doc1.getBugID(), doc2.getBugID(),
                                                       dump));
                        }
                    }

		    public void addResultsToQueue(ArrayList results) {
			boolean done = false;
			while (!done) {
			    try {
				queue.put(results);
				done = true;
			    } catch (InterruptedException e) {
				System.err.println(e.toString());
			    }
			}
		    }
		});
	    
	    executor.execute(t);
	    threadCount++;
	}

	// blocking take
	// once threadCount is 0 we're done!
	int lineCount = 0;
	try {
	    while (threadCount > 0) {
                if (isSample) {
                    ArrayList<String> res = queue.take();
                    for (String line : res) {
                        rd.writeToFile(line);
                        lineCount++;
                    }
                }
                else {
                    ArrayList<REPResults> res = queue.take();
                    /* do logit calcs, buckets */
                    for (REPResults dumpResults: res) {
                        for (String context: dumpResults.results.keySet()) {
                            heap.addToHeap(context, dumpResults.results.get(context), 
                                           dumpResults.id1, dumpResults.id2);
                        }
                        lineCount++;
                    }
		}
		threadCount--;
	    }
	} catch (InterruptedException e) {
	    throw new Error(e.toString());
	}
	executor.shutdown();
        while (!executor.isTerminated()) {
	}

	System.out.println(lineCount + " similarities added.");
    }

    private class REPResults {
        String id1;
        String id2;
        Map<String, Double> results;

        public REPResults(String id1, String id2, Map<String, Double> results) {
            this.id1 = id1;
            this.id2 = id2;
            this.results = results;
        }

    }

}
