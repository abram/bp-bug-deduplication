/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.IdsFilterBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.functionscore.*;
import org.elasticsearch.index.query.functionscore.random.*;
import org.elasticsearch.index.query.QueryBuilders.*;
import org.elasticsearch.common.unit.TimeValue;
import static org.elasticsearch.common.xcontent.XContentFactory.*;
import com.google.gson.*;
import java.util.Random;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

/**
 * Constructs a sample of bugs, where 20% are dupes, and 80% are non-dupes.
 */
public class BugSampler {
    private ElasticSearchManager manager;
    public ArrayList<Document> dupes;
    public ArrayList<Document> nondupes;
    private int target_size;

    public BugSampler(ElasticSearchManager manager, int target_size) {
        this.manager = manager;
	this.target_size = target_size;
    }

    public ArrayList<Document> getNonDupeSample() {
	if (nondupes != null) return nondupes;

	int size = (int) Math.ceil(target_size * 0.8);
        FilterBuilder notDup = FilterBuilders.boolFilter()
            .mustNot(FilterBuilders.termFilter("status", "duplicate"));
        nondupes = getRandomSample(notDup, size);

	return nondupes;
    }

    /*
     * get the dupe sample:
     *
     * pseudo-code:
     * 		sample = get random dupes
     * 		parent_ids = parent ids of dupes in sample
     * 		parent bugs = the bug for each parent id
     *
     * 		dupes = an empty list
     *
     * 		for each dupe in sample:
     * 			if we have its parent:
     * 				add the parent and the dupe to dupes
     * 				(we also keep track of what has been added
     * 				   to make sure we only do so once)
     * 			if we have enough dupes, we can just exit early
     */
    public ArrayList<Document> getDupeSample() {
	    if (dupes != null) {
	        return dupes;
	    }

	    dupes = new ArrayList<Document>();

	    ArrayList<Document> initialDocs = loadInitialDupes();
	    System.out.println("got " + initialDocs.size() + " initial docs");
	    ArrayList<String> parentIDs = getParentIDs(initialDocs);
	    System.out.println("with " + parentIDs.size() + " parent ids");

    	    HashMap<String, Document> parents = getDocs(parentIDs);
	    HashSet<String> includedIDs = new HashSet<String>();

	    int target_dupe_count = (int)Math.ceil(target_size * 0.2);

	    for (Document doc: initialDocs) {
		Document parent = parents.get(doc.getMergeID());
		if (parent == null) continue;

		if (!includedIDs.contains(parent.getBugID())) {
			dupes.add(parent);
			includedIDs.add(parent.getBugID());
		}

		if (!includedIDs.contains(doc.getBugID())) {
			dupes.add(doc);
			includedIDs.add(doc.getBugID());
		}

		if (dupes.size() > target_dupe_count) return dupes;
	    }
	    System.out.println("sampled " + dupes.size() + " dupes.");
	    return dupes;
    }

    ArrayList<Document> loadInitialDupes() {
        FilterBuilder isDup = FilterBuilders.boolFilter()
		.must(FilterBuilders.termFilter("status", "duplicate"))
		.mustNot(FilterBuilders.termFilter("mergeID", "0"));
	return getRandomSample(isDup, target_size);
    }

    /*
     * gets parent ids of all bugs where the parent id is not 0
     */
    ArrayList<String> getParentIDs(ArrayList<Document> docs) {
	   int max_unique = (int) Math.ceil(target_size * 0.2);

	   ArrayList<String> parentIDs = new ArrayList<String>();
	   HashSet<String> allIDs = new HashSet<String>();
	   allIDs.add("0"); // skip docs with no parent

	   for (Document doc : docs) {
		String mergeID = doc.getMergeID();
		if (!allIDs.contains(mergeID)) {
			parentIDs.add(mergeID);
			allIDs.add(mergeID);
		}

		if (parentIDs.size() > max_unique) {
			return parentIDs;
		}
	   }

	   return parentIDs;
    }

    /*
     * get the docs for ids
     */
    HashMap<String, Document> getDocs(ArrayList<String> ids) {
	    HashMap<String, Document> docs = new HashMap<String, Document>();
	    IdsFilterBuilder idFilter = new IdsFilterBuilder("bug");

	    idFilter.addIds(ids.toArray(new String[ids.size()])); 

	    SearchSourceBuilder searchBuilder = new SearchSourceBuilder();
	    searchBuilder.query(QueryBuilders.matchAllQuery());
	    searchBuilder.postFilter(idFilter);
	    searchBuilder.size(ids.size());

	    ArrayList<Document> result = manager.performDocSearch(searchBuilder);
	    for (Document doc : result) {
		docs.put(doc.getBugID(), doc);
	    }

	    return docs;
    }

    public ArrayList<Document> getRandomSample(FilterBuilder dupe_filter, int count) {
        int random = new Random().nextInt();
        ScoreFunctionBuilder score = new RandomScoreFunctionBuilder()
                        .seed(random);

        FunctionScoreQueryBuilder scoreBuilder =
        QueryBuilders.functionScoreQuery(dupe_filter, score);

        SearchSourceBuilder searchBuilder = manager.getPreProcessingFilter(null, scoreBuilder)
            .size(count);

        return manager.performDocSearchNoScroll(searchBuilder);
    }
}
