/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.SearchScroll;
import io.searchbox.core.Search;
import io.searchbox.core.Update;
import io.searchbox.core.Bulk;
import io.searchbox.core.Index;
import io.searchbox.core.Delete;
import io.searchbox.core.DeleteByQuery;
import io.searchbox.params.Parameters;
import io.searchbox.indices.CreateIndex;
import io.searchbox.indices.Analyze;
import io.searchbox.indices.CloseIndex;
import io.searchbox.indices.OpenIndex;
import io.searchbox.Action;
import io.searchbox.BulkableAction;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.common.settings.Settings;

import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.functionscore.*;
import org.elasticsearch.index.query.functionscore.random.*;
import org.elasticsearch.index.query.QueryBuilders.*;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.settings.ImmutableSettings;
import static org.elasticsearch.common.xcontent.XContentFactory.*;
import com.google.gson.*;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Class manages ElasticSearch connection and queries
 * @see ElasticProcessor.java
 */
public class ElasticSearchManager {
    private JestClient client;
    private String project;
    private Bulk.Builder bulk;
    private int bulkSize = 0;
    private int MAXBULK = 50; /* when bulkSize == MAXBULK, perform bulk operation */
    private int SAMPLESIZE = 1000; /* used for 8020 document sample */

    public ElasticSearchManager(String project) {
	JestClientFactory factory = new JestClientFactory();
	factory.setHttpClientConfig(new HttpClientConfig
				    .Builder(getESAddress())
				    .multiThreaded(true)
				    .readTimeout(6000)
				    .build());
	client = factory.getObject();
	this.project = project;
    }

	public static String getESAddress() {
		Map<String, String> env = System.getenv();
       	String host = env.get("ES_PORT_9200_TCP_ADDR");
        if (host == null)
       		host = "127.0.0.1";
		return "http://" + host + ":9200";
	}

    /* Excludes the following bugs from results in addition to any
    *  filters included in the parameter extra: 
    *  bugid == 0
    *  status == 'dupicate' && mergeID == 0
    */
    public SearchSourceBuilder getPreProcessingFilter(FilterBuilder extra) {
        return getPreProcessingFilter(extra, QueryBuilders.matchAllQuery());
    }

    public SearchSourceBuilder getPreProcessingFilter(FilterBuilder extra, 
                                                      QueryBuilder query) {
	FilterBuilder validFilter = FilterBuilders.termFilter
	    ("valid", true);
	SearchSourceBuilder searchBuilder = new SearchSourceBuilder();
	searchBuilder.query(query);
	if (extra != null)
	    searchBuilder.postFilter(extra);
	return searchBuilder;
    }

    public ArrayList<Document> getAllDocsUnfiltered() {
	    SearchSourceBuilder searchBuilder = new SearchSourceBuilder();
	    searchBuilder.query(QueryBuilders.matchAllQuery());

        return performDocSearch(searchBuilder);
    }

    public ArrayList<Document> getAllDocs() {
	SearchSourceBuilder searchBuilder = getPreProcessingFilter(null);

	return performDocSearch(searchBuilder);
    }

    public ArrayList<Document> getAlreadyCalculatedBugs() {
	FilterBuilder ngramExists = FilterBuilders.existsFilter("ngram1");
	SearchSourceBuilder searchBuilder = getPreProcessingFilter(ngramExists);

	return performDocSearch(searchBuilder);
    }

    public ArrayList<Document> getNotCalculatedBugs() {
	FilterBuilder noNgram = FilterBuilders.boolFilter()
	    .mustNot(FilterBuilders.existsFilter("ngram1"));
	SearchSourceBuilder searchBuilder = getPreProcessingFilter(noNgram);

	return performDocSearch(searchBuilder);
    }

    public ArrayList<Document> performDocSearchNoScroll(SearchSourceBuilder searchBuilder) {
        Search search = buildSearch(searchBuilder);
        JestResult result = execute(search);
        return responseToDocument(result);
    }

    private Search buildSearch(SearchSourceBuilder searchBuilder) {
	Search search = new Search.Builder(searchBuilder.toString())
	    .addIndex(project)
	    .addType("bug")
	    .setParameter("_source_exclude", "topics.*")
	    .build();
        
	return search;
    }
    
    public ArrayList<Document> performDocSearch(SearchSourceBuilder 
						 searchBuilder) {
	Search search = buildScrollSearch(searchBuilder);
	JestResult result = execute(search);
	return scrollResponseToDocument(result);
    }

    private Search buildScrollSearch(SearchSourceBuilder searchBuilder) {
	Search search = new Search.Builder(searchBuilder.toString())
	    .addIndex(project)
	    .addType("bug")
	    .setParameter(Parameters.SEARCH_TYPE, "scan")
	    .setParameter(Parameters.SIZE, 500)
	    .setParameter(Parameters.SCROLL, "10s")
	    .setParameter("_source_exclude", "topics.*")
	    .build();
        
	return search;
    }

    private ArrayList<Document> scrollResponseToDocument(JestResult result) {
	ScrollManager scroll = new ScrollManager(result);
	return scroll.getDocsFromScroll();
    }

    private ArrayList<Document> responseToDocument(JestResult result) {
        return (ArrayList<Document>) 
            result.getSourceAsObjectList(Document.class);
    }

    public Boolean setRefreshInterval(int interval) {
	System.out.println("Setting refresh interval to " + interval);
	String settings = "{\"index\" : {\n" +
	    "        \"refresh_interval\" : " + interval + "\n" +
	    "    }}\n";
	Map settingsMap = ImmutableSettings.builder()
	    .loadFromSource(settings).build().getAsMap();
	CreateIndex index = new CreateIndex.Builder(project)
	    .settings(settingsMap).build();

	return execute(index).isSucceeded();
    }

    public Boolean updateDoc(Document toUpdate) {
	String script = getUpdateScript(toUpdate);
	Update update =  getUpdate(script, toUpdate.getBugID(),
                                   "bug");
	return addToBulk(update);
	//return execute(update).isSucceeded();
    }

    public Boolean addSimilarity(String source, String id) {
	Index index = new Index.Builder(source)
	    .index(project)
	    .type("similarities")
            .id(id)
	    .build();
	return addToBulk(index);
	//return execute(index).isSucceeded();
    }

    public Boolean updateSimilarity(String field, double value, String id1, 
				    String id2) {
	StringBuffer script = new StringBuffer("{\"doc\":{");
	script.append("\"" + field + "\":" + value);
	script.append("}, \"upsert\":{");
	script.append("\"" + field + "\":" + value + ",");
	script.append("\"id1\":" + id1 + ",");
	script.append("\"id2\":" + id2 + "}}");
	Update update = getUpdate(script.toString(), id1 + "_" + id2, 
				  "similarities");
	return addToBulk(update);
    }

    public Boolean addToBulk(BulkableAction action) {
	if (bulkSize == 0)
	    bulk = new Bulk.Builder()
		.defaultIndex(project)
		.defaultType("bug");
	bulk.addAction(action);
	bulkSize++;

	if (bulkSize >= MAXBULK) {
	    return executeBulk();
	}
	return true;
    }

    public Boolean executeBulk() {
	if (bulkSize == 0)
	    return true;
	Bulk bulkObj = bulk.build();
	JestResult result = execute(bulkObj);
	bulkSize = 0;
	return result.isSucceeded();
    }

    /* Used in TestElasticSearchManager teardown & TestCleanUp */
    public Boolean deleteCalcFields(Document doc) {
	Boolean success = true;
	String[] fieldsToDelete = {"ngram1", "ngram2", "contextMap"};
	for (String field: fieldsToDelete) {
	    String script = "{\"script\" : \"ctx._source.remove(\\\"" 
		+ field + "\\\")\"}";
	    Update update = getUpdate(script, doc.getBugID(),
				      "bug");
	    success = !addToBulk(update) ? false : success;
	}
	return success;
    }

    public Boolean deleteDoc(String _id, String type) {
	Delete delete = new Delete.Builder(_id)
	    .index(project)
	    .type(type)
	    .build();
	return execute(delete).isSucceeded();
    }

    private String getUpdateScript(Document doc) {
	Gson gson = new Gson();
	String ngram1 = gson.toJson(doc.getNgram1());
	String ngram2 = gson.toJson(doc.getNgram2());
	String contextString = gson.toJson(doc.getContextMap(), 
					   HashMap.class);
	StringBuffer script = new StringBuffer("{");
	script.append("\"doc\" : {");
	script.append("\"ngram1\" : " + ngram1 + ",");
	script.append("\"ngram2\" : " + ngram2 + ",");
	script.append("\"contextMap\" : " + contextString);
	script.append("}}");
		      
	return script.toString(); 
    }

    public Update getUpdate(String script, String _id, String type) {
	Update update = new Update.Builder(script)
	    .index(project)
	    .type(type)
	    .id(_id)
	    .build();
	return update;
    }

    /* Used in TestElasticSearchManager */
    public Boolean deleteSimilarities(int id1, int id2) {
	String query = "{\n" +
	    "    \"query\" : {\n" +
	    "        \"bool\" : { \n" +
	    "          \"must\": [\n" +
	    "            {\"term\": {\"id1\" :" + id1 + " }},\n" +
	    "            {\"term\": {\"id2\" :" + id2 + " }}]\n" +
	    "        }\n" +
	    "    }" +
	    "}";

	DeleteByQuery delete = new DeleteByQuery.Builder(query)
		       .addIndex(project)
		       .addType("similarities")
		       .build();
	JestResult result = execute(delete);
	return result.isSucceeded();
    }

    private JestResult execute(Action clientRequest) {
	JestResult result = null;
	try {
	    result = client.execute(clientRequest);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return result;
    }

    public void shutdown() {
	client.shutdownClient();
    }

    /**
     * Handles elasticsearch scrolling search results
     */
    private class ScrollManager {
	JestResult result;
	String scrollID;

	private ScrollManager(JestResult result) {
	    this.result = result;
	    this.scrollID = result.getJsonObject().get("_scroll_id").getAsString();
	}

	private void setScrollID() {
	    this.scrollID = result.getJsonObject().get("_scroll_id").getAsString();
	}

	private ArrayList<Document> getDocsFromScroll() {
	    ArrayList<Document> allDocs = new ArrayList<Document>();

	    while (true) {
		allDocs.addAll(performScroll());
		if (scrollIsEmpty())
		    break;
	    }
	    
	    return allDocs;
	}

	private Boolean scrollIsEmpty() {
	    return result.getJsonObject().
		getAsJsonObject("hits").
		getAsJsonArray("hits").size() == 0;
	}

	private ArrayList<Document> performScroll() {
	    SearchScroll scroll = new SearchScroll.Builder(scrollID, "5m")
		.setParameter(Parameters.SIZE, 500).build();
	    
	    result = execute(scroll);
	    setScrollID();
	    return (ArrayList<Document>) 
		result.getSourceAsObjectList(Document.class);
	}
    }

}
