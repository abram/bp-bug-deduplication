/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.JsonObject;

import io.searchbox.core.Get;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;

public class CustomTopic {
    public ArrayList<String> query;

    private Corpus corpus;
    private Querier querier;
    private String project;
    private String topic_id;
    private ElasticSearchManager bugs_db;

    public CustomTopic(String project, String topic_id) throws Exception {
	this.topic_id = topic_id;
	this.project = project;
	this.bugs_db = new ElasticSearchManager(project);
	this.query = this.loadQuery();
    }

    public ArrayList<String> loadQuery() throws Exception {
	JestClientFactory factory = new JestClientFactory();
	factory.setHttpClientConfig(new HttpClientConfig
				    .Builder(ElasticSearchManager.getESAddress())
				    .multiThreaded(true)
				    .readTimeout(6000)
				    .build());
		JestClient client = factory.getObject();
        Get get = new Get.Builder(this.project, this.topic_id)
                        .type("topic")
                        .build();
        JestResult result = client.execute(get);
        String words = result.getSourceAsObject(JsonObject.class)
                            .get("words").getAsString().toLowerCase();

        return Util.get_n_grams(words, 1);
    }

    public Map<String, Double> calculateScores() {
	loadCorpus();
	querier = Querier.makeUnigramQuerier(corpus,
					     new ArrayList<String>() 
					     /*no important words */);

	ArrayList<Document> docs = corpus.getDocuments();
	Map<String, Double> scores = new HashMap<String, Double>();
	for (int i = 0; i < docs.size(); i++) {
	    Document doc = docs.get(i);
	    doc.setNgram1(Util.getAllNGrams(doc, 1));
            scores.put("" + doc.getBugID(),
                querier.calculateIntersectBM25F(doc, this.query));
	}
	return scores;
    }

    public void loadCorpus() {
	Preprocessing prep = new Preprocessing();
	ArrayList<Document> docs = bugs_db.getAllDocsUnfiltered();
	prep.process(docs, false);
	corpus = new Corpus(docs);
    }


    public static void main(String[] args) throws Exception {
	String project = args[0];
	String topic_id = args[1];

	CustomTopic topic = new CustomTopic(project, topic_id);
	Map<String, Double> scores = topic.calculateScores();
	StringBuilder builder = new StringBuilder();
        for (String bugid : scores.keySet()) {
            builder.append(bugid);
            builder.append(':');
	    builder.append(scores.get(bugid));
	    builder.append('\n');
	}
		System.out.print(builder.toString());
	}
};
