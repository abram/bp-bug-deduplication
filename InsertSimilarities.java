/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class InsertSimilarities {
    private static String project = "android";
    private static String field = "rep";
    private static BufferedReader reader = null;
    private static String HEADER = "id1,id2,class,REP,cosine_architecture,"
	+ "cosine_nfr,cosine_junk,cosine_lda,cosine_user";
    private static String[] SPLITHEADER = HEADER.split(",");

    private static void initFileReader() {
	File file = new File("/csv_files/" + project + "/" + field + ".csv");
	try {
	    /* create new filewriter that appends */
	    FileReader fr = new FileReader(file);
	    reader = new BufferedReader(fr);
	}
	catch (IOException e) {
	    e.printStackTrace();
	}
    }

    private static void readFileAndInsert() {
	ElasticSearchManager es = new ElasticSearchManager(project);
	/* for each line in file, ignoring first */
	try {
	    String line = reader.readLine();
	    while ((line = reader.readLine()) != null) {
		String[] lineSplit = line.split(",");
		int len = lineSplit.length;
		double similarity = Double.parseDouble(lineSplit[len - 1]);
		es.updateSimilarity(field, similarity,
				    lineSplit[0], lineSplit[1]);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	/* execute any leftover actions in bulk */
	es.executeBulk();
	es.shutdown();
	System.out.println("Similarities from " + field + " added");
    }


    /**
     * Builds the insert json for the REPDumper's dump
     * Not currently used
     */
    private static String buildDumpJson(String line) {
	String[] splitLine = line.split(",");
	StringBuffer json = new StringBuffer("{");
	int len = splitLine.length;
	for (int i = 0; i < len - 1; i++) {
	    json.append(SPLITHEADER[i] + ":" + splitLine[i]);
	    json.append(",");
	}
	json.append(SPLITHEADER[len - 1] + ":" + splitLine[len - 1]);
	json.append("}");
	return json.toString();
    }

    public static void main(String[] args) {
	if (args.length != 2)
	    System.out.println("Usage is <project> <field>. " +
			       "Ex. java InsertSimilarities android all_norep");
        project = args[0];
	field = args[1];

	initFileReader();
	readFileAndInsert();
    }
}