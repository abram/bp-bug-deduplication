PROCESSOR=${1:-"android"}
FILE="$PROCESSOR.csv"
echo $FILE
export CLASSPATH=./:./NFR.EXP2:./lib/*:/usr/share/java/junit-4.11.jar:.:/usr/share/java/hamcrest-core-1.3.jar:.:./issues:./tests

java Main --project="$PROCESSOR" --output=/csv_files/"$FILE" -c -p
cd ../fastrep
sudo bash runOut.sh /csv_files/"$FILE"
