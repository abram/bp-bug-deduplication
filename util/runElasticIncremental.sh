PROCESSOR=${1:-"android"}
FILE="$PROCESSOR.csv"

export CLASSPATH=./:./NFR.EXP2:./lib/*:/usr/share/java/junit-4.11.jar:.:/usr/share/java/hamcrest-core-1.3.jar:.:./issues:./tests

echo "Calling incremental bug-dedup"

java Main --project="$PROCESSOR" --output=/csv_files -p
