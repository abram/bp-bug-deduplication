import urllib2
import json
import couchdb

def dumptojsonfile( filename, data):
    file = open(filename, "w")
    file.write(json.dumps(data, indent=2))
    file.close()

def connect_to_couchdb():
    couch = couchdb.Server()
    db = couch['android']
    return db

db = connect_to_couchdb()

url = "http://localhost:5984/android/_all_docs?include_docs=true"
data = json.loads(urllib2.urlopen(url).read())

dumptojsonfile("issues/json_issues.txt", data)
