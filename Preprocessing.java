/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.*;
import java.util.Arrays;
import java.util.*;
import java.util.Map;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.xml.soap.Text;

public class Preprocessing {
    public static enum DupeStrategy {
	    RM_DUPES, IGNORE_DUPES
    };

    private DupeStrategy dupeStrategy;
    private HashSet<String> stopWords = null;

    public Preprocessing(DupeStrategy strategy) {
        this.dupeStrategy = strategy;
    }

    public Preprocessing() {
        this(DupeStrategy.RM_DUPES);
    }

    public ArrayList<Document> process(ArrayList<Document> input, 
				       boolean stemming) {
	Map<String, Document> idDocMap = new Corpus(input).getIdDocMap();
	ArrayList<Document> result = new ArrayList<Document>();
	for (Document tf : input) {
	    tf = isJunkDocument(idDocMap, tf);
            if (tf == null || tf.getBugID().equals("0"))
                continue;
	    tf = removeAllJunkChars(tf);
	    tf = allStopWordsRemoval(tf);
            
	    if (stemming) {
            tf = stemmingAll(tf);
            tf = allStopWordsRemoval(tf);
	    }

	    String newDesc = tf.getDescription().replaceAll("(\\s)+", " ");
	    String newTitle = tf.getTitle().replaceAll("(\\s)+", " ");
	    tf.setTitle(newTitle.toLowerCase());
	    tf.setDescription(newDesc.toLowerCase());

	    result.add(tf);
	}
    return result;
    }

    public Document isJunkDocument(Map<String, Document> idDocMap, 
				  Document doc) {
	if (dupeStrategy == DupeStrategy.IGNORE_DUPES) {
		return doc;
	}

	if (doc.getBugID().equals("0"))
	    return null;

	if (doc.getStatus().equals("duplicate")) {
	    if (doc.getMergeID().equals("0") ||
		!idDocMap.containsKey(doc.getMergeID()))
		return null;
		
	    Document master = getMasterDoc(idDocMap, doc);
	    if (master.getStatus().equals("duplicate") ||
		doc.compareTo(master) < 1)
		return null;
	}
	return doc;
    }

    private Document getMasterDoc(Map<String, Document> idDocMap,
				  Document doc) {
	Document master = idDocMap.get(doc.getMergeID());
	
	while (master.getStatus().equals("duplicate")
	       && !master.getMergeID().equals("0")
	       && idDocMap.containsKey(master.getMergeID())) {
	    master = idDocMap.get(master.getMergeID());
	}
	return master;
    }

    public Document removeAllJunkChars(Document input) {
	String newDesc = removeJunkChars(input.getDescription());
	String newTitle = removeJunkChars(input.getTitle());
	input.setDescription(newDesc);
	input.setTitle(newTitle);
	return input;
    }

    public String removeJunkChars(String input) {
	String result = "";
	result = input.replaceAll("Q'E", "'");
	result = result.replaceAll("Q\"E", "\"");
	result = result.replaceAll("Q>E", ">");
	result = result.replaceAll("Q<E", "<");

	return result;
    }

    public Document allStopWordsRemoval(Document doc) {
	String newDescription = stopWordsRemoval(doc.getDescription());
	String newTitle = stopWordsRemoval(doc.getTitle());
	doc.setDescription(newDescription);
	doc.setTitle(newTitle);
	return doc;
    }

    public String stopWordsRemoval(String input) {
	String result = "";
	String output = "";

	if (stopWords == null) {
	    initStopWords();
	}

	String[] inputString = input.split(" ");
	ArrayList<String> outputString = new ArrayList<String>();

	for (String word : inputString) {
	    if (!stopWords.contains(word.toLowerCase()))
		outputString.add(word);
	}

	for (String s : outputString)
	    output += " " + s;

	inputString = output.split("\\W");
	for (String word : inputString) {
	    if (!stopWords.contains(word.toLowerCase()))
		result += " " + word;
	}
	return result;
    }

    private void initStopWords() {
	try {
	    FileInputStream fstream = new FileInputStream("issues/stop words.txt");

	    DataInputStream in = new DataInputStream(fstream);
	    BufferedReader br = new BufferedReader(
						   new InputStreamReader(in));
	    String strLine;
	    stopWords = new HashSet<String>();

	    while ((strLine = br.readLine()) != null) {
		stopWords.add(strLine);
	    }
	    in.close();
	} catch (IOException e) {
	    throw new Error(e.toString());
	}
    }

    public Document stemmingAll(Document doc) {
	Pattern p = Pattern.compile("[^A-Za-z]");
	String[] splittedDesc = p.split(doc.getDescription());
	String newDescription = "";
	for (String descWord : splittedDesc)
	    newDescription += " " + Stemmer.stem(descWord);
	
	String[] splittedTitle = p.split(doc.getTitle());
	String newTitle = "";
	for (String titleWord : splittedTitle)
	    newTitle += " " + Stemmer.stem(titleWord);
	
	doc.setDescription(newDescription);
	doc.setTitle(newTitle);
	return doc;
    }
}
