import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.text.ParseException;

import java.util.HashMap;
import java.util.Map;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;


public class TestLogit {
    private static Logit logit;
    private static File logitTestFile;
    private static File defaultLogitTestFile;

    @BeforeClass
    public static void setup() {
        logit = new Logit("tests", "./");
        setupLogitFiles();
    }

    @AfterClass
    public static void teardown() {
        logitTestFile.delete();
        //defaultLogitTestFile.delete();
    }

    @Test
    public void testGetLogitFileNameREP() {
        String filename = logit.getLogitFileName("REP", true);
        assertEquals("8020.REP...lmfit-coef.csv", filename);
    }

    @Test
    public void testGetLogitFileNameALL() {
        String filename = logit.getLogitFileName("ALL", true);
        assertEquals(filename, "8020.ALL.0.1.lmfit-coef.csv");

        filename = logit.getLogitFileName("ALL", false);
        assertEquals(filename, "8020.ALL.1.1.lmfit-coef.csv");
    }

    @Test
    public void testGetLogitFileNameCosine() {
        String filename = logit.getLogitFileName("cosine_user", true);
        assertEquals(filename, "8020.cosine_user...lmfit-coef.csv");

        filename = logit.getLogitFileName("cosine_user", false);
        assertEquals(filename, "8020.cosine_user.1..lmfit-coef.csv");
    }

    @Test
    public void testReadLogitFile() {
        Map<String, Double> result = logit.readLogitFile("testLogit.txt");
        assertEquals(result.get("cosine_user"), null);
        assertEquals(result.get("Intercept"), -8.25452, 0.002);
        assertEquals(result.get("REP"), -0.7907, 0.002);
    }

    @Test
    public void testReadLogitFileDoesntExist() {
        Map<String, Double> result = logit.readLogitFile("doesntExist.txt");
        assertEquals(result.get("cosine_user"), 0.0001, 0.00001);
        assertEquals(result.get("Intercept"), -10.25452, 0.002);
        assertEquals(result.get("REP"), -2.7907, 0.002);
    }

    @Test
    public void testPerformLogit() {
        Map<String, Double> coeff = new HashMap<String, Double>();
        coeff.put("Intercept", 2.02);
        coeff.put("REP", 10.01);
        coeff.put("cosine_user", 3.03);
        logit.coefficients = new HashMap<String, Map<String, Double>>();
        logit.coefficients.put("cosine_user", coeff);

        Map<String, Double> cosines = new HashMap<String, Double>();
        cosines.put("REP", 1.0);
        cosines.put("cosine_user", 1.2);
        cosines.put("notInCoeffFile", 0.0);

        Double userLogit = logit.performLogit(cosines, "cosine_user");
        assertEquals(0.99999984284, userLogit, 0.0000000001);
    }

    private static void setupLogitFiles() {
        File dir = new File("tests/lmfit");
        File defaultDir = new File("./default");
        mkdir(dir);
        mkdir(defaultDir);

        logitTestFile = new File("tests/lmfit/testLogit.txt");
        defaultLogitTestFile = new File("./default/doesntExist.txt");
        BufferedWriter output = mkfile(logitTestFile);
        BufferedWriter defaultOutput = mkfile(defaultLogitTestFile);

	try {
	    output.write("\"\",\"x\"\n" +
                         "\"(Intercept)\",-8.25452146847384\n" +
                         "\"REP\",-0.790700754758838\n" +
                         "\"cosine_user\",NA\n");
            output.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}

        try {
            defaultOutput.write("\"\",\"x\"\n" +
                                "\"(Intercept)\",-10.25452146847384\n" +
                                "\"REP\",-2.790700754758838\n" +
                                "\"cosine_user\",0.0001\n");
            defaultOutput.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    private static void mkdir(File dir) {
        if (!dir.exists()) {
            try{
                dir.mkdir();
            } catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private static BufferedWriter mkfile(File file) {
        BufferedWriter output = null;
        try {
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            output = new BufferedWriter(fw);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }
}