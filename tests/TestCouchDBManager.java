import com.google.gson.JsonObject;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class TestCouchDBManager {
    private static CouchDBManager couchdb;
    private final ByteArrayOutputStream errContent = 
	new ByteArrayOutputStream();

    @Before
    public void setup() {
	System.setErr(new PrintStream(errContent));
	couchdb = new CouchDBManager("mini_android");
    }

    @After
    public void teardown() {
	couchdb.shutDownCouchdb();
    }

    @Test
    public void testgetAllDocs() {
	ArrayList<Document> result = couchdb.getAllDocs();
	assertEquals(131, result.size());
    }

    @Test
    public void testGetAlreadyCalculated() {
	ArrayList<Document> result = couchdb.getAlreadyCalculatedBugs();
	assertEquals(0, result.size());
    }

    @Test
    public void testGetNotCalculated() {
	ArrayList<Document> result = couchdb.getNotCalculatedBugs();
	assertEquals(130, result.size());
    }

    public void testUpdateNgrams() {
	/* not entirely sure how to test this... */
    }
}