import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;

import java.util.ArrayList;
import java.util.Arrays;


public class TestCustomTopic {
	@Test
	public void testSimpleRanking() {
        	// issue 1 : "title": "the SVN repository is empty.", 
		CustomTopic topic = topicForWords("the SVN repository is empty");
		double[] scores = topic.calculateScores();

		assertEquals(scores.length, 4);
		for (double score : scores) {
			assertTrue(score >= 0);
			assertTrue("the document whose title we are"
				+ " querying with should have the highest"
				+ " score", score <= scores[1]);
		}
	}

	public CustomTopic topicForWords(String words) {
		return new CustomTopic("issues/small_json_issues.txt",
			Util.get_n_grams(words, 1));
	}
}
