import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;

public class TestPreprocessing {
    static ArrayList<Document> allDocs = new ArrayList<Document>();
    Preprocessing prep = new Preprocessing();
    static Document doc = new Document();
    /* remove because id = 0 */
    static Document doc2 = new Document();
    static Document dup = new Document();
    /* remove because mergeID doen't exist */
    static Document dup2 = new Document();
    /* remove because dup3.openDate < doc.openDate */
    static Document dup3 = new Document();

    @BeforeClass
    public static void setup() {
	doc.setBugID(1);
	doc.setTitle("two title");
	doc.setDescription("description a stop");
	doc.setOpenDate("2012-01-01T24:24:24.000Z");

	doc2.setBugID(0);

	dup.setBugID(2);
	dup.setStatus("duplicate");
	dup.setMergeID(1);
	dup.setOpenDate("2013-01-01T24:24:24.000Z");

	dup2.setBugID(3);
	dup2.setStatus("duplicate");
	dup2.setMergeID(5);

	dup3.setBugID(4);
	dup3.setStatus("duplicate");
	dup3.setMergeID(1);
	dup3.setOpenDate("2011-01-01T24:24:24.000Z");
	
	allDocs.add(doc);
	allDocs.add(doc2);
	allDocs.add(dup);
	allDocs.add(dup2);
	allDocs.add(dup3);
    }

    @Test
    public void testProcess() {
	prep.process(allDocs, false);
	assertEquals(" title", allDocs.get(0).getTitle());
	assertEquals(" description stop", allDocs.get(0).getDescription());
    }

    @Test
    public void testRemoveJunkChars() {
	String testStr = "lahbdididly.>.235..//?";
	String result = prep.removeJunkChars(testStr);
	assertEquals(testStr, result);

	testStr = "blahQ>E";
	result = prep.removeJunkChars(testStr);
	assertEquals("blah>", result);
    }

    @Test
    public void testStopWordsRemoval() {
	String testStr = "a all don't ";
	String result = prep.stopWordsRemoval(testStr);
	assertEquals(" ", result);

	testStr = "a .bunch of junkwords inthis string";
	result = prep.stopWordsRemoval(testStr);
	assertEquals("   bunch junkwords inthis string", result);
    }
    
    @Test
    public void testAllStopWordsRemoval() {
	Document doc = new Document();
	doc.setTitle("a all don't");
	doc.setDescription("Android, junkwords string");
	
	doc = prep.allStopWordsRemoval(doc);
	assertEquals(" ", doc.getTitle());
	assertEquals("  android  junkwords string", doc.getDescription());
    }
}