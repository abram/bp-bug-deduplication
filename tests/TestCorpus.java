import java.util.ArrayList;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;

public class TestCorpus {
    static ArrayList<Document> allDocs = new ArrayList<Document>();
    static Document doc = new Document();
    static Document dup = new Document();
    static Document dup2 = new Document ();

    @BeforeClass
    public static void setup() {
	doc.setBugID(1);
	doc.setStatus("New");
	doc.setOpenDate("2012-01-01T24:24:24.000Z");

	dup.setBugID(2);
	dup.setStatus("duplicate");
	dup.setMergeID(1);
	dup.setOpenDate("2013-01-01T24:24:24.000Z");

	dup2.setBugID(3);
	dup2.setStatus("duplicate");
	dup2.setMergeID(4);
	dup2.setOpenDate("2013-01-01T24:24:24.000Z");

	allDocs.add(doc);
	allDocs.add(dup);
	allDocs.add(dup2);
    }
}