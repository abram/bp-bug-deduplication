import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.PriorityQueue;

public class TestHeap {
    private Heap heap;

    @Before
    public void setUp() {
        heap = new Heap(5);
    }
    
    @After
    public void tearDown() {
        heap = null;
    }

    @Test
    public void testInsertElemEmptyHeap() {
        Boolean success = heap.addToHeap("REP", 1.0, "id1", "id2");

        assertTrue(success);
        assertEquals(heap.heaps.get("REP").size(), 1);
        assertEquals(heap.heaps.get("REP").peek().value, 1.0, 0.001);
    }

    @Test
    public void testInsertLessThanMin() {
        Double[] sampleList = {1.0, 1.1, 1.2, 1.3, 1.5};
        for (Double sample: sampleList) {
            heap.addToHeap("REP", sample, "id1", "id2");
        }

        Boolean success = heap.addToHeap("REP", 0.9, "id1", "id2");
        assertFalse(success);
        assertEquals(heap.heaps.get("REP").peek().value, 1.0, 0.001);
    }

    @Test
    public void testInsertMoreThanMin() {
        Double[] sampleList = {1.0, 1.1, 1.2, 1.3, 1.5};
        for (Double sample: sampleList) {
            heap.addToHeap("REP", sample, "id1", "id2");
        }

        Boolean success = heap.addToHeap("REP", 1.01, "1", "2");
        assertTrue(success);
        assertEquals(heap.heaps.get("REP").peek().value, 1.01, 0.001);
        assertEquals(heap.heaps.get("REP").size(), 5);
    }

}