import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

public class TestElasticSearchManager {
    private static ElasticSearchManager es;
    private static Document doc;
    private static Document doc1;
    private static ArrayList<Document> allDocs = new ArrayList<Document>();

    @BeforeClass
    public static void setup() {
        TestCleanUp tc = new TestCleanUp();
        tc.cleanBugs("mini_android");

        es = new ElasticSearchManager("mini_android");
    }

    @AfterClass
    public static void teardown() {
        TestCleanUp tc = new TestCleanUp();
        tc.cleanBugs("mini_android");

        es.shutdown();
    }

    @Test
    public void testGetAllDocs() {
        ArrayList<Document> docs = es.getAllDocs();
	
        assertEquals(130, docs.size());
    }

    @Test
    public void testGetAlreadyCalculatedBugs() {
        ArrayList<Document> docs = es.getAlreadyCalculatedBugs();
        assertEquals(0, docs.size());
    }

    @Test
    public void testGetNotCalculatedBugs() {
        ArrayList<Document> docs = es.getNotCalculatedBugs();

        assertEquals(130, docs.size());
    }

    @Test
    public void testupdateDocs() {
	ArrayList<String> ngram = new ArrayList<String>();
	ngram.add("one");
	ngram.add("two");

	double[] NFR = {0.01, 0.02};
	Map<String, double[]> contextMap = new HashMap<String, double[]>();
	contextMap.put("NFR", NFR);

	doc = new Document();
	doc.setBugID(1);
	doc.setNgram1(ngram);
	doc.setContextMap(contextMap);

	Boolean success = es.updateDoc(doc);
	assertEquals(true, success);
	
	success = es.executeBulk();
	assertEquals(true, success);
	
	/* cleanup */
	success = es.deleteCalcFields(doc);
	//success = es.deleteDoc("1_2", "similarities");
	if (!success) 
	    System.out.println("Error: Teardown was not successful");
    }


    @Test
    public void testAddSimilarity() {
	StringBuffer source = new StringBuffer("{");
	source.append("\"id1\": " + 0 + ",");
	source.append("\"id2\": " + 0 + ",");
	source.append("\"is_duplicate\": " + false + ",");
	source.append("\"REP\": " + 0 + ",");
	source.append("\"cosine_arch\": " + 0 + ",");
	source.append("\"cosine_nfr\": " + 0 + ",");
	source.append("\"cosine_junk\": " + 0 + ",");
	source.append("\"cosine_lda\": " + 0 + ",");
	source.append("\"cosine_user\": " + 0 + "}");

	Boolean success = es.addSimilarity(source.toString(), "0_0");
	assertEquals(success, true);

	success = es.executeBulk();
	assertEquals(success, true);
	/* cleanup */
	es.deleteSimilarities(0, 0);
    }

    @Test
    public void testUpdateSimilarityDoesntExist() {
	double myDouble = 2.90070146550427e-12;
	Boolean success = es.updateSimilarity("testField", 
					      myDouble, 
					      "1", "2");
	es.executeBulk();
	assertEquals(success, true);

	/* cleanup */
	es.deleteSimilarities(1, 2);
    }

    @Test
    public void testUpdateSimilarityAlreadyExists() {
	double myDouble = 2.90070146550427e-12;
	Boolean success = es.updateSimilarity("testField2", myDouble,
					      "1", "2");
	success = es.updateSimilarity("testField3", myDouble, 
					      "1", "2");
	es.executeBulk();
	assertEquals(success, true);

	/* cleanup */
	es.deleteSimilarities(1, 2);
    }
}