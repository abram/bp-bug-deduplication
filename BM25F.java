/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class BM25F {
    Tfidf tfidf;
    private int n;
    ArrayList<String> important_words;
    private boolean reweighting;
    private Pattern pattern;

    public BM25F(Corpus corps, int input_n_gram, boolean stemming,
		 ArrayList<String> importantWords) {
	n = input_n_gram;
	tfidf = new Tfidf(corps, n);
	this.reweighting = reweighting;
	this.important_words = importantWords;
    }

    public int countFrequencies(String str, String word) {
	Pattern pattern = Pattern.compile(word);
	Matcher matcher = pattern.matcher(str);
	int counter = 0;
	while (matcher.find())
	    counter++;
	return counter;
    }

    public double WQ(int titleWordCount, int descWordCount, 
		     double wdesc, double wsumm, double k3) {
	double TFQ1 = wsumm * titleWordCount;
	double TFQ2 = wdesc * descWordCount;
	double TFQ = TFQ1 + TFQ2;
	return (double) ((k3 + 1) * TFQ) / (k3 + TFQ);
    }

    public double calculateBM25F(Document document, Set<String> intersection,
				 Querier.FreeVariables freeVariables) {

	double result = 0;
	for (String t : intersection) {
	    int titleWordCount = countFrequencies(document.getTitle(), t);
	    int descWordCount = countFrequencies(document.getDescription(), t);
	    double IDF = tfidf.calculateIDF(t);
	    double TFD = tfidf.calculateTfD(document, titleWordCount,
					    descWordCount,
					    freeVariables.wsumm,
					    freeVariables.wdesc,
					    freeVariables.bdesc,
					    freeVariables.bsumm);
	    double WQ = WQ(titleWordCount, descWordCount,
		    		freeVariables.wdesc,
		    		freeVariables.wsumm,
		    		freeVariables.k3);

	    result += ((this.reweighting && important_words.contains(t)) ? 1000
		       : 1)
		* IDF
		* (TFD / (freeVariables.k1 + TFD))
		* WQ;

	}
	return result;
    }    
}
