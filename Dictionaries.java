/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.File;

public class Dictionaries {
    private static boolean stemming;
    private ArrayList<ArrayList<String>> NFR;
    private ArrayList<ArrayList<String>> junk;
    private ArrayList<ArrayList<String>> arch;
    private ArrayList<ArrayList<String>> LDA;
    private ArrayList<ArrayList<String>> user;
    private ArrayList<String> importantWords;

    public Dictionaries(String projectName, String userDir, boolean stemming,
			String importantWordsFile) {
	this.stemming = stemming;
	this.NFR = initNFRDictionaries();
	this.junk = initJunkDictionaries();
	this.arch = initArchDictionaries(projectName);
	this.LDA = initLDADictionaries(projectName);
	this.user = initUserDictionaries(userDir);
	this.importantWords = initImportantWords(importantWordsFile);
    }

    private String getLDADir(String parserName) {
	return "lda/" + getDictDir(parserName);
    }

    private String getArchDir(String parserName) {
	return "architecture/" + getDictDir(parserName);
    }

    private String getDictDir(String parserName) {
	if (parserName.equals("AndroidXml"))
	    return "android/";
	else if (parserName.equals("EclipseXml"))
	    return "eclipse/";
	else if (parserName.equals("Json") ||
		 parserName.equals("CouchDBParser"))
	    return "json/";
	else if (parserName.equals("MozillaXml"))
	    return "mozilla/";
	else if (parserName.equals("OpenofficeXml"))
	    return "openoffice/";
	else
	    return "";
    }

    public ArrayList<ArrayList<String>> getNFR() {
	return this.NFR;
    }

    public ArrayList<ArrayList<String>> getJunk() {
	return this.junk;
    }

    public ArrayList<ArrayList<String>> getArch() {
	return this.arch;
    }

    public ArrayList<ArrayList<String>> getLDA() {
	return this.LDA;
    }

    public ArrayList<ArrayList<String>> getUser() {
	return this.user;
    }

    public ArrayList<String> getImportantWords() {
	return this.importantWords;
    }

    private ArrayList<ArrayList<String>> initNFRDictionaries() {
	String[] dictionaryFiles = { "NFR.EXP2/wordlist.efficiency",
				     "NFR.EXP2/wordlist.functionality", 
				     "NFR.EXP2/wordlist.maintainability",
				     "NFR.EXP2/wordlist.portability", 
				     "NFR.EXP2/wordlist.reliability",
				     "NFR.EXP2/wordlist.usability" };
	ArrayList<Document> dict = initDictionaries(dictionaryFiles);
	return getDictNGrams(dict);
    }

    private ArrayList<ArrayList<String>> initJunkDictionaries() {
	char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
	String[] dictionaryFiles = new String[alphabet.length];

	for (int i = 0; i < alphabet.length; i++) {
	    dictionaryFiles[i] = "junk/" + alphabet[i] + "_rand_100.txt";
	}
	ArrayList<Document> dict = initDictionaries(dictionaryFiles);
	return getDictNGrams(dict);
    }

    private ArrayList<ArrayList<String>> initArchDictionaries(String parserName) {
	String dir = getArchDir(parserName);
	if (dir.equals("architecture/")) {
	    System.err.println("There is no matching architecture dictionary"
			       + " directory.");
	    return new ArrayList<ArrayList<String>>();
	}
	int numDict = initNumDict(dir);

	String[] dictionaryFiles = new String[numDict];
	for (int i = 0; i < numDict; i++) {
	    dictionaryFiles[i] = dir + "topic" + i + ".txt";
	}
	ArrayList<Document> dict = initDictionaries(dictionaryFiles);
	return getDictNGrams(dict);
    }

    private ArrayList<ArrayList<String>> initLDADictionaries(String parserName) {
	String dir = getLDADir(parserName);
	if (dir.equals("lda/")) {
	    System.err.println("There is no matching LDA dictionary"
			       + " directory.");
	    return new ArrayList<ArrayList<String>>();
	}

	int numDict = initNumDict(dir);
	
	String[] dictionaryFiles = new String[numDict];
	for (int i = 0; i < numDict; i++) {
	    dictionaryFiles[i] = dir + "topic" + i + ".txt";
	}
	ArrayList<Document> dict = initDictionaries(dictionaryFiles);
	return getDictNGrams(dict);
    }

    private ArrayList<ArrayList<String>> initUserDictionaries(String userDir) {
	if (userDir.isEmpty())
	    return new ArrayList<ArrayList<String>>();
	int numDict = initNumDict(userDir);

	String[] dictionaryFiles = new String[numDict];
	for (int i = 0; i < numDict; i++) {
	    dictionaryFiles[i] = userDir + "/"
		+ "topic" + i + ".txt";
	}
	ArrayList<Document> dict = initDictionaries(dictionaryFiles);
	return getDictNGrams(dict);
    }

    private int initNumDict(String dictDir) {
	String workingDir = System.getProperty("user.dir");
	int numDict = 0;
	try {
	    numDict = new File(workingDir + "/" + dictDir + "/")
		.listFiles().length;
	} catch (Exception e) {
	    String errorMessage = "Error: " + dictDir + " does not exist.";
	    System.err.println(errorMessage);
	    throw new RuntimeException(errorMessage, e);
	}
	return numDict;
    }

    private ArrayList<Document> initDictionaries(String[] dictionaryFiles) {
	ArrayList<Document> dictionaries = new ArrayList<Document>();
		
	for (String file: dictionaryFiles) {
	    ArrayList<String> tmp = readImportantWordsFile(file);
	    StringBuffer buff = new StringBuffer();
	    for (String word: tmp) {
		buff.append(word);
		buff.append(" ");
	    }
	    String out = buff.toString();
	    Document d = new Document();
	    d.setTitle(out);
	    d.setDescription(out);
	    dictionaries.add(d);
	}

	return dictionaries;
    }

    private ArrayList<ArrayList<String>> getDictNGrams(ArrayList<Document> dict) {
	ArrayList<ArrayList<String>> allNGrams = 
	    new ArrayList<ArrayList<String>>();
	ArrayList<String> ngram = null;
	for (Document doc: dict) {
	    ngram = Util.getAllNGrams(doc, 1);
	    allNGrams.add(ngram);
	}
	return allNGrams;
    }

    private static ArrayList<String> initImportantWords(String fileName) {
	if (!fileName.isEmpty())
	    return readImportantWordsFile(fileName);
	else
	    return new ArrayList<String>();
    }

    public static ArrayList<String> readImportantWordsFile(String fileName) {
	ArrayList<String> result = new ArrayList<String>();
	try {
	    // Open the file that is the first
	    // command line parameter
	    FileInputStream fstream = new FileInputStream(fileName);
	    // Get the object of DataInputStream
	    DataInputStream in = new DataInputStream(fstream);
	    BufferedReader br = new BufferedReader(new InputStreamReader(in));
	    String strLine;
	    // Read File Line By Line
	    while ((strLine = br.readLine()) != null) {
		// Print the content on the console
		result.add(stemming ? Stemmer.stem(strLine) : strLine);
	    }
	    // Close the input stream
	    in.close();
	} catch (Exception e) {// Catch exception if any
	    String errorMessage = "All dictionaries must be named "
		   + "topic*.txt where * is a sequence from 0 to number of"
		   + " dictionaries - 1";
	    System.err.println(errorMessage);
	    System.err.println(fileName);
	    throw new RuntimeException(errorMessage, e);
	}
	return result;
    }

}
