JFLAGS = -g
JC = javac
.SUFFIXES: .java .class
CLASSPATH = ./:./NFR.EXP2:./lib/*:/usr/share/java/junit-4.11.jar:.:/usr/share/java/hamcrest-core-1.3.jar:.:./issues:./tests

.java.class:
	@$(JC) -cp $(CLASSPATH) $(JFLAGS) $*.java

CLASSES = \
	BM25F.java \
	BugSampler.java \
	Corpus.java \
	CouchDBManager.java \
	CustomTopic.java \
	Dictionaries.java \
	Document.java \
	ElasticSearchManager.java \
	ElasticProcessor.java \
	Heap.java \
	IncrementalProcessor.java \
	InsertSimilarities.java \
	InsertSimsOneUpdate.java \
	Logit.java \
	Main.java \
	Parser.java \
	Preprocessing.java \
	Processor.java \
	Querier.java \
	REPDumper.java \
	REP.java \
	Stemmer.java \
	TestCleanUp.java \
	Tfidf.java \
	Util.java \

default: classes

classes: $(CLASSES:.java=.class)

clean: clean_tests
	@$(RM) *.class

clean_tests:
	@cd tests; make clean

Tests: classes tests
	@cd tests; make

runTests: classes Tests
	@java -ea -cp $(CLASSPATH) org.junit.runner.JUnitCore TestBM25F TestLogit TestHeap TestElasticProcessor TestCustomTopic TestCouchDBManager TestElasticSearchManager TestIncrementalProcessor TestPreprocessing TestREP TestREPDumper TestUtil TestBugSampler

runIncrement: classes
	@java -ea -cp $(CLASSPATH) org.junit.runner.JUnitCore TestCouchDBManager TestIncrementalProcessor

# specify project, topic on commandline, eg.
# make project=android topic=0 custom-topic-analysis
custom-topic-analysis: classes
	@java -ea -cp $(CLASSPATH) CustomTopic $(project) $(topic)

runElastic: Tests
	@java -ea -cp $(CLASSPATH) org.junit.runner.JUnitCore TestElasticSearchManager TestElasticProcessor

runLogit: Tests
	@java -ea -cp $(CLASSPATH) org.junit.runner.JUnitCore TestLogit

runHeap: Tests
	@java -ea -cp $(CLASSPATH) org.junit.runner.JUnitCore TestHeap