Copyright
=========

Copyright (C) 2013 Anahita Alipour, Abram Hindle, Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

Usage
=====
     -c,--couchdb                       Use couchdb as backend. Default is ElasticSearch.
     -h,--help                          Prints argument descriptions
        --importantWords FILENAME>      Filename with a list of important words
        --output DIRECTORY>             Output directory
     -p,--parallel                      Perform bug comparisons in parallel
        --project NAME>                 Project name Ex. 'EclipseXML' or
                                        'android'. REQUIRED.
     -s,--stemming                      Perform stemming on all words
        --userDictionary DIR>           Name of directory containing user
                                        defined dictionaries

Branches
========
* master: a combination of couchdb and elasticsearch, but some small bugs exist
* couchDbRelease: uses couchDB as it's backend. Should be bug-free.
* elasticsearch: uses elasticsearch as it's backend. Should be bug-free.

Overview
=========
The bug-deduplication program calculates several similarity measures between all bugs in a given project.  The top 10,000 similar bug-pairs for each similarity measure  are put in elasticsearch.

This program's execution is dependent on another repo called [FastREP](https://bitbucket.org/abram/fastrep/overview). You must use the elasticsearch branch.

Logic
-----
A sample of bugs is taken from a project.  Bug-deduplication is performed on the sample, which produces a &lt;project name>.csv file which contains the following columns:

* id1
* id2
* class: either "dup" or "non" where "dup" indicates a pair of duplicate bugs. 
  * For example, bug1.bugid = 100, bug2.bugid = 200, bug2.status = "duplicate" and bug2.mergeID = 100
* REP: a similarity measure
* cosine_architecture: a similarity measure using a dictionary of project-specific common architectural words.
* cosine_nfr: a similarity measure using a dictionary of non-functional requirement words.
* cosine_junk: a similarity measure using a dictionary of junk words.
* cosine_lda: a similarity measure using a dictionary of project-specific Latent Dirichlet Allocation (LDA)  words.
* cosine_user: a similarity measure using a dictionary of project-specific user-chosen words.

The dictionaries mentioned above are located in the &lt;dictionary name>/&lt;project name> directory of the bug-deduplication repo.  

Next, the <project name>.csv file is run through FastREP to get a logistical regression model for each similarity measure.  The model files produced by FastREP are stored in the <output>/<project name>/lmfit directory, where <output> is the output directory defined when running bug-deduplication.  Once these models are calculated, the bug-deduplication program is run on all of the projects bugs, and each bug-pair similarity is pushed through the model and a logit function.  The top 10,000 similar bugs for each similarity measure are put in elasticsearch.

If bug-deduplication is run without a sample, and the FastREP project-specific model files do not exist, then default model files are used.  The default model files should be located in the <output>/default directory, where <output> is the directory defined when running bug-deduplication.

If bug-deduplication is run and there are no "dup" pairs, then the default model will be used.

Diagram
-------
              _ _ _ _ _ 
            |           |
            | isSample? |
            | _ _ _ _ _ |
    
              /      \
             / Yes    \ No
      _ _ _ _             _ _ _ _  
    |         |         |         |
    |   get   |         | project |
    | sample  |         |  model  |
    | _ _ _ _ |         | exists? |
                        | _ _ _ _ |
         |               
      _ _ _ _ _          /       \
    |           |       / Yes     \ No
    |    run    |  _ _ _ _ _       _ _ _ _
    | bug-dedup | |           |  |         |
    | _ _ _ _ _ | |    use    |  |   use   |
                  |  project  |  | default |
         |        |   model   |  |  model  |
      _ _ _ _ _   | _ _ _ _ _ |  | _ _ _ _ |
    |          |        \            /
    |   run    |         \          /
    | FastREP  |           _ _ _ _ _
    | _ _ _ _ _|         |           |
                         |    run    |
         |               | bug-dedup |
     _ _ _ _ _ _ _ _ _   | _ _ _ _ _ |
    |                  |
    |    run again     |
    | isSample = false |
    | _ _ _ _ _ _ _ _ _|


Scripts
=======
* util/runElastic.sh: Use this to run bugdedup + fastrep from scratch.  A new logistic model will be created, and all bugs will be recalculated.
* util/runElasticIncremental.sh: Use this to run incremental bugdedup + fastrep.  The logistic model from the original run will be used.


Bug Fields
==========
Necessary
---------
* bugid
* description
* mergeID
* status
* title

Used but not necessary
----------------------
* type
* component
* priority
* version