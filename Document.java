/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Document implements Comparable<Document> {
    private String _rev = "";
    private String _id = "";
    private String description = "";
    private String title = "";
    private String status = "";
    private String bugid = "0";
    private String component = "";
    private String priority = "";
    private String type = "";
    private String version = "";
    private String mergeID = "0";
    private String openedDate = "";
    private String dateFormat = "yyyy-MM-dd'T'HH:mm:ss'.000Z'";
    private List<String> ngram1 = new ArrayList<String>();
    private List<String> ngram2 = new ArrayList<String>();
    private Map<String, double[]> contextMap = new HashMap<String, double[]>();

    private transient Map<String, Integer> priorityMap = 
	new HashMap<String, Integer>() {
	{
	    /* Android Priorities */
	    put("Small", 1);
	    put("Medium", 2);
	    put("High", 3);
	    put("Critical", 4);
	    put("Blocker", 5);

	    /* Mozilla Priorities */
	    put("--",0);
	    put("",0);
	    /* Eclipse Priorities */
	    put("P1",1);
	    put("P2",2);
	    put("P3",3);
	    put("P4",4);
	    put("P5",5);
	}
    };

    public Document() {

    }

    public void setOpenDate(String openDate) {
	this.openedDate = openDate;
    }

    public Date getOpenDate() {
	if (openedDate.equals("") || openedDate.equals("null"))
	    return new Date();
	else
	    return getDate(openedDate);

    }

    private Date getDate(String inputDate) {
	Date date = null;
        String[] dateFormats = {"EEE, d MMM yyyy HH:mm:ss Z",
                                "yyyy-MM-dd'T'HH:mm:ss'.000Z'",
                                "yyyy-MM-dd'T'HH:mm:ss",
                                "yyyy-MM-dd HH:mm:ss Z",
                                "yyyy-MM-dd HH:mm:ss a",
                                "dd/MM/yyyy HH:mm:ss a"};
        inputDate = inputDate.replace(" Americas/Edmonton", "");
        for (String format: dateFormats) {
            date = parseDate(inputDate, format);
            if (date != null)
                return date;
        }
	return date;
    }

    private Date parseDate(String inputDate, String dateFormat) {
	SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
	Date date = new Date();

	try {
	    date = sdf.parse(inputDate);
	    String GMTTime = date.toGMTString();
	    date = new Date(GMTTime);
	} catch (ParseException e) { 
            return null;
        }
	return date;       
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description.toLowerCase();
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title.toLowerCase();
    }

    public String getBugID() {
	return bugid;
    }

    public void setBugID(String bugID) {
	this.bugid = bugID;
    }

    public void setBugID(int bugID) {
        this.bugid = Integer.toString(bugID);
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getComponent() {
	return (component == null? "" : component);
    }

    public void setComponent(String component) {
	this.component = component;
    }

    public int getPriorityNumber(String priority) {
	if (priority == null || priority.equals(""))
	    return -1;

	return priorityMap.get(priority);
    }

    public int getPriority() {
	return getPriorityNumber(this.priority);
    }

    public void setPriority(String priority) {
	this.priority = priority;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getVersion() {
	if (this.version.equals(""))
	    return "-1";

	if (this.version.startsWith("r"))
	    return version.substring(1);

	if (version.equals("OpenSource"))
	    return "0";
	else 
	    return version;
    }

    public void setVersion(String version) {
	this.version = version;
    }

    public String getMergeID() {
	return mergeID;
    }

    public void setMergeID(String mergeID) {
        this.mergeID = mergeID;
    }

    public void setMergeID(int mergeID) {
	this.mergeID = Integer.toString(mergeID);
    }

    public void setDateFormat(String dateFormat){
	this.dateFormat = dateFormat;
    }
	
    public String getDateFormat(){
	return dateFormat;
    }

    public void setNgram1(List<String> ngram1) {
	this.ngram1 = ngram1;
    }

    public ArrayList<String> getNgram1() {
	return new ArrayList(this.ngram1);
    }

    public void setNgram2(ArrayList<String> ngram2) {
	this.ngram2 = ngram2;
    }

    public ArrayList<String> getNgram2() {
	return new ArrayList(this.ngram2);
    }

    public void setContextMap(Map<String, double[]> map) {
	this.contextMap = map;
    }

    public Map<String, double[]> getContextMap() {
	return this.contextMap;
    }

    @Override
	public int compareTo(Document doc2) {
	// TODO Auto-generated method stub
	if (this.getOpenDate().after(doc2.getOpenDate()))
	    return 1;
	else if (this.getOpenDate().before(doc2.getOpenDate()))
	    return -1;

	return 0;
    }

    @Override
    public String toString() {
	return "_id: " + _id + " _rev: " + _rev + "bugid: " + bugid 
	    + " title: " + title + " status: " + status 
	    + " mergeID: " + mergeID + " component: " + component 
	    + " openDate: " + openedDate 
	    + " priority: " + getPriorityNumber(priority) + " type: " + type
	    + " version: " + version;
    }
}
