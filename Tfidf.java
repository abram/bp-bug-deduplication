/*
 * Code that applies machine learning concepts to improve
 * bug deduplication accuracy in bug repositories.
 * Copyright (C) 2013  Anahita Alipour, Abram Hindle,
 * Tanner Rutgers, Riley Dawson, Finbarr Timbers, Karan Aggarwal
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tfidf {
    Map<String, Integer> dictionary = new HashMap<String, Integer>();
    int numDocs;
    private Corpus corps;

    public Tfidf(Corpus corp, int input_n) {
	ArrayList<Document> documents = corp.getDocuments();
	createDictionary(documents, input_n);
	numDocs = documents.size();
	corps = corp;
    }

    public void setCorps(Corpus corps) {
	this.corps = corps;
    }

    public Corpus getCorps() {
	return corps;
    }

    public void createDictionary(ArrayList<Document> corpus, int n) {
	for (Document doc : corpus) {

	    Set<String> temporarySet = new HashSet<String>();
	    temporarySet.addAll(Util.get_n_grams(doc.getDescription(), n));
	    temporarySet.addAll(Util.get_n_grams(doc.getTitle(), n));
	    temporarySet.remove("");

	    for (String word : temporarySet) {
		if (dictionary.containsKey(word))
		    dictionary.put(word, dictionary.get(word) + 1);
		else
		    dictionary.put(word, 1);
	    }
	}
    }

    public double calculateTfD(Document d, int titleWordCount,
			       int descWordCount, double wsumm,
			       double wdesc, double bdesc, double bsumm) {
	double TF1 = (double) (wsumm * titleWordCount)
	    / (1 - bsumm + (double) (bsumm * d.getTitle().length())
	       / corps.getAvgTitleLength());
	double TF2 = (double) (wdesc * descWordCount)
	    / (1 - bdesc + (double) (bdesc * d.getDescription().length())
	       / corps.getAvgDescriptionLength());
	return TF1 + TF2;
    }

    public double calculateIDF(String t) {
	/*if (dictionary.get(t) == null) {
	    System.err.println("t: " + t);
	    return 0;
	    }*/
	return Math.log10((double) numDocs / dictionary.get(t));
    }

    public int getTextWordsFreq(String text, String word) {
	Pattern pattern = Pattern.compile(word);
	Matcher matcher = pattern.matcher(text);
	int counter = 0;

	while (matcher.find())
	    counter++;

	return counter;
    }
}
